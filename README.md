# Sources:

 - https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/GettingStarted
 - https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/HelloWorld

# Install

```
$ cd ~/kloudyornot/plasmoid
$ kpackagetool5 -t Plasma/Applet --install .
$ kpackagetool5 -t Plasma/Applet --upgrade .
$ kpackagetool5 -t Plasma/Applet --remove org.kde.kloudyornot
```

# Testing

```
kpackagetool5 -t Plasma/Applet --upgrade .
```
Does not work until I reboot. Then, I seem to have multiple instances on my desktop, and I delete them with long right click, but they seem to come back after a reboot.
 - <d_ed> it probably would work after a "plasmashell --replace"
 - <d_ed> you might also want to check out plasmoidviewer which can restart easily

# Web Requests
I think my next step would be to try to invoke some kind of script, say to curl from a website or to cat /etc/os-release. Is there a guide to that kind of thing.
 - <d_ed> https://doc.qt.io/qt-5/qtqml-javascript-qmlglobalobject.html#xmlhttprequest    if you need web requests
 - <d_ed> But sometimes there are specific QML APIs available
 - <d_ed> I'm sure there's one that reads /etc/os-release